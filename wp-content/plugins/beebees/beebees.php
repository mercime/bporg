<?php
namespace beebees;

use function beebees\settings\is_buddypress_dot_org;

/**
 * Plugin Name: 🐝🐝
 * Description: A beehive full of tweaks for bbPress/BuddyPress.org.
 * Version:     1.0
 * License:     GPLv3 or later.
 * Author:      Paul Gibbs
 */

require_once __DIR__ . '/libs/extended-cpts/extended-cpts.php';
require_once __DIR__ . '/libs/extended-taxos/extended-taxos.php';

require_once __DIR__ . '/settings.php';  // I go first.
require_once __DIR__ . '/content-types.php';
require_once __DIR__ . '/redirects.php';
require_once __DIR__ . '/performance.php';
require_once __DIR__ . '/seo.php';
require_once __DIR__ . '/ux.php';


// Site specific includes.
if ( is_buddypress_dot_org() ) {
	require_once __DIR__ . '/buddypress-org.php';
} else {
	require_once __DIR__ . '/bbpress-org.php';
}
