<?php
namespace beebees\settings;

/**
 * Settings.
 */

add_action( 'admin_init', __NAMESPACE__ . '\\add_site_settings' );

/**
 * Add custom options to wp-admin Settings.
 */
function add_site_settings() {
	// Site type.
	register_setting( 'general', 'beebees_site_type', function( $new_value ) {
		if ( ! in_array( $new_value, [ 'bbpress', 'buddypress' ], true ) ) {
			return 'buddypress';
		}

		return $new_value;
	} );

	add_settings_field(
		'beebees_site_type',
		'Site Type',
		function() {
			?>
			<select name="beebees_site_type" id="beebees_site_type">
				<option value="bbpress" <?php selected( 'bbpress', get_site_type() ); ?>>bbpress.org</option>
				<option value="buddypress" <?php selected( 'buddypress', get_site_type() ); ?>>buddypress.org</option>
			</select>
			<p class="description">
				<label for="beebees_site_type">Is the current site like buddypress.org, or more like bbpress.org?</label></p>
			<?php
		},
		'general'
	);


	// Root forum.
	register_setting( 'general', 'beebees_root_forum', function( $new_value ) {
		if ( ! in_array( $new_value, array_keys( get_forum_slugs_and_names() ), true ) ) {
			return '';
		}

		return $new_value;
	} );

	add_settings_field(
		'beebees_root_forum',
		'Display Forum',
		function() {
			$current_selected = get_root_forum();
			?>
			<select name="beebees_root_forum" id="beebees_root_forum">
				<option value="" <?php selected( '', $current_selected ); ?>>(none)</option>
				<?php foreach ( get_forum_slugs_and_names() as $forum_slug => $forum_name ) : ?>
					<?php
					printf(
						'<option value="%1$s" %2$s>%3$s</option>',
						esc_attr( $forum_slug ),
						selected( $current_selected, $forum_slug, false ),
						esc_html( $forum_name )
					);
					?>
				<?php endforeach; ?>
			</select>
			<p class="description">
				<label for="beebees_root_forum">Use this as the forum to display like Stackoverflow.</label></p>
			<?php
		},
		'general'
	);
};

/**
 * Is the current site like buddypress.org, or more like bbpress.org?
 *
 * @return string Either "buddypress" or "bbpress".
 */
function get_site_type() {
	return get_option( 'beebees_site_type', 'buddypress' );
}

/**
 * Is the current site buddypress.org?
 *
 * @return bool
 */
function is_buddypress_dot_org() {
	return ( 'buddypress' === get_site_type() );
}

/**
 * Is the current site bbpress.org?
 *
 * @return bool
 */
function is_bbpress_dot_org() {
	return ( 'bbpress' === get_site_type() );
}

/**
 * Get the root forum (displays it as a Stackoverflow-style).
 *
 * @return string
 */
function get_root_forum() {
	return get_option( 'beebees_root_forum', '' );
}

/**
 * Get all forums' slugs and names.
 *
 * @return array Key is slug, name is value.
 */
function get_forum_slugs_and_names() {
	if ( ! function_exists( '\bbp_get_forum_post_type' ) ) {
		return [];
	}

	$forums = get_posts( [
		'no_found_rows'          => true,
		'post_type'              => bbp_get_forum_post_type(),
		'posts_per_page'         => 50,
		'update_post_meta_cache' => false,
		'update_post_term_cache' => false,
	] );

	$retval = [];
	foreach ( $forums as $forum ) {
		$retval[ "{$forum->post_name}" ] = "{$forum->post_title}";
	};

	return $retval;
}
