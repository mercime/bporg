<?php
namespace beebees\buddypress;

/**
 * Odds and sods that are specific for BuddyPress.org.
 */

/**
 * Switch database encoding.
 */
add_action( 'init', function() {
	if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
		return;
	}

	// Set dbhs to an empty array - unsetting it causes headaches.
	$GLOBALS['wpdb']->dbhs    = array();
	$GLOBALS['wpdb']->charset = 'utf8';
}, 0 );

/**
 * Filter forum template parts to support our "root forum" option (displays it as a Stackoverflow-style).
 *
 * @param array       $template
 * @param string      $slug
 * @param string|null $name
 *
 * @return array
 */
 add_filter( 'bbp_get_template_part', function( $templates, $slug, $name = null ) {
	if (
		! function_exists( '\beebees\settings\get_root_forum' ) ||
		! \beebees\settings\get_root_forum() ||
		$slug !== 'loop' && $name !== 'forums'
	) {
		return $templates;
	}

	array_unshift( $templates, 'loop-forums-stackoverflow.php' );
	return $templates;
}, 10, 3 );
